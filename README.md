# Desafio 1 – Estágio em desenvolvimento PHP Foco Multimídia

### How to use:

1.  `git clone https://gitlab.com/fernandesd/desafiohotel.git` or  [Download](https://gitlab.com/fernandesd/desafiohotel/-/archive/master/desafiohotel-master.zip)
2.  `cd desafiohotel`
3.  `php -S localhost:8000 -t public`
4.  [http://localhost:8000](http://localhost:8000)