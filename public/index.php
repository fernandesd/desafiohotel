
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="@fernandesd">

  <title>Hotel Jamaica</title>

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="assets/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

  <!-- Custom styles for this template -->
  <link href="assets/css/grayscale.css" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><strong style="color:#d8db16;">H</strong>otel <strong style="color:#36b528;">J</strong>amaica</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#reserva"><strong>Reservar</strong></a>
          </li> 

        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 " id="typed"></h1><br>
        <a href="#reserva" class="btn btn-primary js-scroll-trigger">Faça a sua reserva</a>
      </div>
    </div>
  </header>

  <!-- Reserva Section -->
  <section id="reserva" class="projects-section bg-light">
    <div class="container d-flex align-items-center">
      <div class="mx-auto text-center"> 
        <div class="card text-white bg-success mb-3" style="max-width: 28rem;">
          <div class="card-header"><strong> Fazer Reserva </strong><i class="fas fa-calendar-check"></i></div>
          <div class="card-body">

            <!-- Reserva Form -->
            <form action="reserva.php" method="post">
              <div class="form-row">
                <div class="col">
                  <input type="text" class="form-control" name="name" placeholder="Nome" required>
                </div>
              </div>
              <br>
              <div class="form-row">
                <div class="col">
                  <label for="entrada">Check-in <i class="far fa-calendar-alt"></i></label>
                  <input type="date" name="startDate_at" class="form-control" id="entrada" required>
                </div>

                <div class="col">
                  <label for="saida">Check-out <i class="far fa-calendar-alt"></i></label>
                  <input type="date" name="endDate_at" class="form-control" required>
                </div> 
              </div>
              <br>

              <div class="form-row">
                <div class="col">
                  <label for="quartoType">Tipo de quarto</label>
                  <select class="form-control" name="quartoType" id="quartoType" required>                    
                    <option value="0">Individual</option>
                    <option value="1">Duplo</option>
                  </select>
                </div>
                <div class="col">
                  <label for="pessoas">Pessoas <i class="fas fa-users"></i></label>
                  <input type="number"name="people" class="form-control" id="pessoas" placeholder="Pessoas" required>
                </div>
                <div class="col">
                  <label for="quartos">Quartos <i class="fas fa-bed"></i></label>
                  <input type="number" name="bedroom" class="form-control" id="quartos" placeholder="Quartos" required>
                </div>
              </div><br>
              <input type="submit" class="btn btn-warning" name="submit" value="Enviar">

            </form>
          </div>
        </div>

      </div>
    </div>
  </section> 


  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      &copy; Diego Fernandes 2019
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="assets/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="assets/jquery-easing/jquery.easing.js"></script>

  <!-- Typed.js -->
  <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>

  <!-- Inicializando Typed.js-->
  <script>
    var typed = new Typed('#typed', {
      strings: ['Um paraíso feito para você!'],
      typeSpeed: 70,
      loop:true,
      backSpeed:50,
      startDelay:500
    });
  </script>

  <script src="assets/js/animeForm.js"></script>
  <!-- Custom scripts for this template -->
  <script src="assets/js/grayscale.min.js"></script>

</body>

</html>
