<?php

if(isset($_POST['submit'])){


	//Sanitizações e Validações
	$erros = [];
	$nome = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
	if (filter_input(INPUT_POST, 'people') < 0 || filter_input(INPUT_POST, 'bedroom') < 0){
		
		$erros[] = "<div style='color:red;'>Campo não pode ter valores negativos, seu espertinho c(: </div><br> <a href='index.php#reserva'>Voltar</a>";
		if(!empty($erros)){		
			foreach ($erros as $erro) {
				echo "<div class='alert alert-danger' role='alert'>
				$erro <br>
				</div>";
			}
		}
	}else{


	setlocale(LC_MONETARY,"pt_BR.UTF-8");


	//Obtendo os dias de hospedagem
		$data1 = new DateTime($_POST['startDate_at']);
		$data2 = new DateTime($_POST['endDate_at']);

		$intervalo = $data1->diff($data2);

	//periodo em dias
		$periodo = $intervalo->d;

	//Obtendo o valor da diária
		$valorDiaria = $_POST['quartoType'] == '0' ?  100 : 150;
		$valorDiaria *= $periodo;


	//Obtendo taxa de turismo
		$taxaTurismo =  10*$periodo*$_POST['people'];

	//Obtendo taxa de wifi
		$wifi = 20*$periodo*$_POST['bedroom'];

		$total = $valorDiaria+$taxaTurismo+$wifi;

	//Calculando a taxa de serviço
		$iss = $total*0.05*$_POST['bedroom'];

		$total+=$iss;

		?>

		<!doctype html>
		<html lang="pt-BR">
		<head>
			<!-- Required meta tags -->
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">		

			<!-- Bootstrap CSS -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

			<!-- Fontawesome CSS -->
			<link href="assets/fontawesome-free/css/all.min.css" rel="stylesheet">

			<!-- Animate CSS -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

			<title>HJ - Reserva	</title>
		</head>
		<body style="background: linear-gradient(to bottom, rgba(22, 22, 22, 0.3) 0%, rgba(22, 22, 22, 0.7) 75%, #161616 100%), url('assets/img/banner.jpg');background-position: center;background-attachment: scroll;background-size: cover;background-repeat: no-repeat;">
			<div class="container d-flex align-items-center" style="height: 100vh;">
				<div class="mx-auto"> 
					<div class="list-group animated tada slower" style="width: 20rem">
						<button type="button" class="list-group-item  list-group-flush list-group-item-action active">
							<?php echo "Olá, {$nome}! Aqui está a sua ficha: "; ?>
						</button>
						<button type="button" class="list-group-item list-group-item-action"><i class="fas fa-calendar-day"></i> <?php echo "Diárias - R$ " . number_format($valorDiaria, 2, ',', '.')  ; ?></button>
						<button type="button" class="list-group-item list-group-item-action"><i class="fas fa-hotel"></i> <?php echo "Taxa de serviço - R$ " . number_format($iss, 2, ',', '.')  ; ?></button>
						<button type="button" class="list-group-item list-group-item-action"><i class="fas fa-camera-retro"></i> <?php echo "Taxa de turismo - R$ " . number_format($taxaTurismo, 2, ',', '.')  ; ?></button>
						<button type="button" class="list-group-item list-group-item-action"> <i class="fas fa-wifi"></i> <?php echo "Taxa wifi - R$ " . number_format($wifi, 2, ',', '.')  ; ?></button>
						<button type="button" class="list-group-item list-group-item-action"><i class="fas fa-money-check"></i> <strong><?php echo "TOTAL: R$ " . number_format($total, 2, ',', '.')  ; ?></strong></button>
					</div>
				</div>
			</div>


			<!-- Optional JavaScript -->
			<!-- jQuery first, then Popper.js, then Bootstrap JS -->
			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

		</body>
		</html>

		<?php
	}
}else
echo '<a href="index.php#reserva">Ainda não fez a reserva? Clique aqui para acessar o formulário!</a>'



?>
