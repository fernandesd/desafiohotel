

const name = document.getElementsByName('name')[0];
name.addEventListener('focus', addClassName, false);
name.addEventListener('blur', removeClassName, false);

function addClassName(){	
	name.classList.add("animated");
	name.classList.add("pulse");
}

function removeClassName(){	
	name.classList.remove("animated");
	name.classList.remove("pulse");
}

const people = document.getElementsByName('people')[0];
people.addEventListener('focus', addClassPeople, false);
people.addEventListener('blur', removeClassPeople, false);

function addClassPeople(){	
	people.classList.add("animated");
	people.classList.add("swing");
}

function removeClassPeople(){	
	people.classList.remove("animated");
	people.classList.remove("swing");
}

const bedroom = document.getElementsByName('bedroom')[0];
bedroom.addEventListener('focus', addClassBedroom, false);
bedroom.addEventListener('blur', removeClassBedRoom, false);

function addClassBedroom(){	
	bedroom.classList.add("animated");
	bedroom.classList.add("swing");
}

function removeClassBedRoom(){	
	bedroom.classList.remove("animated");
	bedroom.classList.remove("swing");
}

const submit = document.getElementsByName('submit')[0];
submit.addEventListener('mouseenter', addClassSubmit, false);
submit.addEventListener('mouseleave', removeClassSubmit, false);

function addClassSubmit(){	
	submit.classList.add("animated");
	submit.classList.add("pulse");
	submit.classList.add("infinite");
}

function removeClassSubmit(){	
	submit.classList.remove("animated");
	submit.classList.remove("pulse");
	submit.classList.remove("infinite");
}

